# Obesityデータセット

## 概要

南米で調査された，食習慣・生活習慣と肥満との関係に関するデータ。

田鎖が改変（2021年）。

## 出典

### データセット

UCI Machine Learning Repositoryの次の項目

Estimation of obesity levels based on eating habits and physical condition Data Set ( https://archive.ics.uci.edu/ml/datasets/Estimation+of+obesity+levels+based+on+eating+habits+and+physical+condition+# )

### 詳細

Fabio Mendoza Palechor and Alexis de la Hoz Manotas. (2019). Dataset for estimation of obesity levels based on eating habits and physical condition in individuals from Colombia, Peru and Mexico ( https://www.sciencedirect.com/science/article/pii/S2352340919306985?via%3Dihub )

## 変数の説明

`age`および`BMI`以外はカテゴリ変数である。

 - `age`：年齢
 - `freq_vegetable`：野菜を摂る頻度
 - `n_meal`：1日の食事回数
 - `freq_food_between_meal`：間食の頻度
 - `amount_water`：水分の摂取量
 - `freq_phys`：運動の頻度
 - `time_tec_device`：デジタル機器の使用時間
 - `amount_alcohol`：アルコールの摂取量
 - `transport`：交通手段
 - `BMI`：Body Mass Index

## 改変

 - 統計解析の練習用に，データを一部抜粋した
 - 欠測を補ったと思われるデータについては，カテゴリ変数とするために四捨五入した。