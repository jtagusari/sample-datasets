# Cardiovascular Disease Dataset
## Fields
- Age | Objective Feature | age | int (days)
- Height | Objective Feature | height | int (cm) |
- Weight | Objective Feature | weight | float (kg) |
- Gender | Objective Feature | gender | categorical code | 1: women, 2: men
- Systolic blood pressure | Examination Feature | ap_hi | int |
- Diastolic blood pressure | Examination Feature | ap_lo | int |
- Cholesterol | Examination Feature | cholesterol | 1: normal, 2: above normal, 3: well above normal |
- Glucose | Examination Feature | gluc | 1: normal, 2: above normal, 3: well above normal |
- Smoking | Subjective Feature | smoke | binary (whether patient smokes) | 0: No, 1: Yes
- Alcohol intake | Subjective Feature | alco | binary (whether patient takes alcohol)| 0: No, 1: Yes
- Physical activity | Subjective Feature | active | binary (whether patient performs physical activity) | 0: No, 1: Yest
- Presence or absence of cardiovascular disease | Target Variable | cardio | binary (whether cardiovascular disease is present) | 0: No, 1: Yes

All of the dataset values were collected at the moment of medical examination. 

## NOTE
- Objective: factual information;
- Examination: results of medical examination;
- Subjective: information given by the patient.