---
title: "Nonparametric maximum likelihood estimation for random effect model"
author: "Junta Tagusari"
output: 
  html_document:
    toc: true
    toc_depth: 2
    toc_float:
      collapsed: false
    number_sections: true
editor_options: 
  chunk_output_type: console
---


# なにがしたいか
 - 同モデル，パッケージ`npmlreg`( https://cran.r-project.org/web/packages/npmlreg/vignettes/npmlreg-v.pdf )の写経
 - 変量効果を自由度少なめの固定効果でモデル化したり，混合ガウス分布でモデル化できたりする

# 設定

## ライブラリ
```{r loadlib, message = F}
library(tidyverse)
library(npmlreg)
library(MASS)
```


## セッション情報
```{r sessioninfo}
sessionInfo()
Sys.time()
```

# 説明変数なし`galaxy`データ

## データ読み込み

```{r}
data(galaxies, package = "MASS")
galaxies[78] <- 26960

gal <- galaxies %>% 
  as_tibble() %>% 
  dplyr::mutate(value = value / 1000)

summary(gal)
```

プロット

```{r}
ggplot(gal, aes(value)) +
  geom_histogram(boundary = 0, binwidth = 1)+
  theme_bw()
```

おおよそ20近辺に固まっているが，10近辺に固まっているものや30以上あるものもある。

## `glm`

何の変哲もない平均値モデリング
```{r}
glm(value ~ 1, data = gal) %>% summary()
```

## NPML変量効果モデル

適当な`k: mass point`の設定のもと，変量効果を推定する。
`k=1`では，固定効果モデルと一致し，それ以外ではEMアルゴリズムを使って変量効果を求める。

```{r}
gal_model_np <- purrr::map(
  1:25,
  ~alldist(value~1, random = ~1, data = gal, k = .x, plot.opt = 0, random.distribution = "np") 
)
```

### `mass point`のプロット

```{r}
data_mp <- purrr::map_dfr(
  gal_model_np,
  ~tibble(
    mass = .x$mass.points,
    weight = .x$masses,
    k = length(.x$mass.points)
  )
)

ggplot(data_mp)+
  geom_point(aes(k, mass, size = weight), shape = 21) +
  scale_x_continuous("Number of mass points", breaks = seq(0,20,2))+
  scale_y_continuous("Value of mass points", breaks = seq(0,50,5))+
  coord_cartesian(xlim = c(0,16), ylim = c(5,35))+
  theme_bw()
```

### EMアルゴリズム収束のプロット

```{r}
data_em <- purrr::map_dfr(
  gal_model_np,
  ~.x$Misc$EMTrajectories %>% 
    as_tibble() %>% 
    dplyr::mutate(iteration = row_number(), k = length(.x$mass.points))
) %>% 
  tidyr::pivot_longer(starts_with("MASS")) %>% 
  na.omit()

ggplot(dplyr::filter(data_em, k <= 10))+
  geom_line(aes(iteration, value, group = name)) +
  facet_wrap(~k)+
  scale_x_continuous("Iteration", breaks = seq(0,50,10))+
  scale_y_continuous("Value of mass points", breaks = seq(0,50,5))+
  coord_cartesian(xlim = c(0,50), ylim = c(5,35))+
  theme_bw()
```

### `disparity`の比較

disparityは周辺尤度$L$に関して$-2\log L$で定義される。
ベイズモデルでは周辺尤度を最大にするモデルを選択すべき（要勉強）だから，disparityが最小となるモデルを選択する。

```{r}

data_d <- purrr::map_dfr(
  gal_model_np,
  ~tibble(
    k = length(.x$mass.points),
    disparity = .x$disparity,
    # deviance = .x$deviance
  )
) 

ggplot(data_d)+
  geom_line(aes(k, disparity)) +
  geom_point(aes(k, disparity)) +
  scale_x_continuous("Number of mass points", breaks = seq(0,30,5))+
  scale_y_continuous("Disparity", breaks = seq(300,500,20))+
  theme_bw()
```

### 予測と残差

```{r}
data_pred <- purrr::map_dfr(
  gal_model_np,
  ~tibble(
    k = length(.x$mass.points),
    value = gal$value,
    predict = predict(.x),
    residual = abs(predict - value)
  )
) 


ggplot(dplyr::filter(data_pred, k <= 8))+
  geom_point(aes(value, residual))+
  facet_wrap(~k) +
  scale_x_continuous("Values", breaks = seq(0,30,5))+
  scale_y_continuous("Residuals", breaks = seq(0,15,5))+
  coord_cartesian(xlim = c(0,35), ylim = c(0,15))+
  theme_bw()
```

### 固定効果との関係

モデルの中の`lastglm$weights`をEMアルゴリズムによって求めている。
この$weights$を使って，縦に引き延ばしたデータを使って`glm.fit`を実行し，`mass.point`を求めている。
（さらに，これを使って`weights`を更新することを繰り返す）

だから，適当なデータと`weights`を使って`glm.fit`を行えば，同じ結果を得る。


NPMLの結果
```{r}
gal_model_np[[3]]
```

縦に引き延ばしたデータ
```{r}

gal_x3 <- gal %>% .[rep(1:nrow(.), 3),] %>% 
  cbind(
    mass1 = c(rep(1,1/3*nrow(.)), rep(0,2/3*nrow(.))), 
    mass2 = c(rep(0,1/3*nrow(.)), rep(1,1/3*nrow(.)), rep(0,1/3*nrow(.))), 
    mass3 = c(rep(0,2/3*nrow(.)), rep(1,1/3*nrow(.)))
    )

head(gal_x3)
```

縦に引き延ばしたデータのglm計算結果。NPMLと一致する。
```{r}
glm(value ~.-1, data = gal_x3, weights = gal_model_np[[3]]$lastglm$weights) %>% 
  summary()
```

### 固定効果モデルへの変換

重みづけglmはglmと似て非なるもので，標準誤差や信頼区間の計算は正しくない（おそらく，重みづけ推定値の信頼区間が算出できる）。
ただし，NPMLの計算は，離散分布に収束するはずなので，重みづけglmをglmに変換できるはず。


重みづけは，ほぼ1,0,0，の組になっている
```{r}
matrix(gal_model_np[[3]]$lastglm$weights, ncol = 3) %>% 
  apply(1, max) %>%
  .[] - 1
```

1か0に離散化してglmを求めると，NPMLと近い結果が得られる。
```{r}
glm_model_np_3 <- glm(value~.-1, 
    data = cbind(gal, mass = factor(apply(matrix(gal_model_np[[3]]$lastglm$weights, ncol = 3), 1, which.max)))) 

summary(glm_model_np_3)

summary(gal_model_np[[3]])
```

NPMLの収束をもっと厳しくしてやれば，glmの結果に近づくはずだが，正確には一致しない。

```{r}
gal_model_np_3_rev <- alldist(
  value~1, random = ~1, data = gal, k = 3, plot.opt = 0, random.distribution = "np", 
  EMdev.change = 1e-7, spike.protect = 1
  ) 

summary(gal_model_np_3_rev)
```

### ブートストラップ法による信頼区間の推定

```{r}
data_list <- purrr::map(
  1:2000, ~gal[sample(seq(1,nrow(gal)), replace = T),]
)

model_list <- purrr::map(
  data_list, 
  ~alldist(
    value~1, random = ~1, data = .x, k = 3, plot.opt = 0, random.distribution = "np", verbose = F
  ) 
)

df_mass <- purrr::map_dfr(model_list, ~as_tibble_row(coef(.x))) %>% 
  dplyr::mutate(model = row_number()) %>% 
  tidyr::pivot_longer(-model)

ggplot(df_mass, aes(value)) +
  geom_histogram(boundary = 0, binwidth = 0.05)+
  facet_wrap(~name, scale = "free_x")
```

#### パーセンタイル法で信頼区間を求める

最も簡単で仮定も少ないが，精度が低いといわれる。
95%信頼区間を求めるためには2.5パーセンタイルを求める必要がある。

```{r}
df_mass %>% 
  tidyr::nest(mass_est = c(model, value)) %>% 
  dplyr::mutate(
    qt005 = purrr::map_dbl(mass_est, ~quantile(.x$value, 0.005)),
    qt025 = purrr::map_dbl(mass_est, ~quantile(.x$value, 0.025)),
    qt975 = purrr::map_dbl(mass_est, ~quantile(.x$value, 0.975)),
    qt995 = purrr::map_dbl(mass_est, ~quantile(.x$value, 0.995)),
  )
```

#### BC_a法

パーセンタイル法においては，パラメータの分布が原点に対して対象となるような変換が存在することを仮定している。
BC_a法では，より緩い仮定のもとで信頼区間を求める。



## 混合ガウス分布

ガウス分布だと，こうなる

```{r}
gal_model_gauss <- alldist(value ~ 1, random = ~1, k = 4, data = gal, lambda = 1)
gal_model_gauss
```

# `respiratory``データ

 - `centre`：施設カテゴリ
 - `subject`：患者カテゴリ
 - `visit`：測定時点カテゴリ
 - `treatment`：治療群カテゴリ
 - `status`：呼吸器の状態カテゴリ
 - `gender`：性別カテゴリ
 - `age`：年齢
 - `post`：治療期間（本質的にはカテゴリだが，`glmer`の制約で連続値とした）

## データ読み込み

```{r}
data("respiratory", package = "HSAUR2") 

data <- as_tibble(respiratory) %>% 
  dplyr::transmute(
    num = row_number(),
    centre,
    subject,
    visit = factor(month, ordered = F),
    post = as.numeric(visit != "0"),
    treatment,
    status,
    gender,
    age,
    age_cen = scale(age)
  )


summary(data)
data
```


## データ概観

治療群の方が状態はよさそうだが。。

```{r}
ggplot(data)+
  geom_jitter(aes(visit, status, fill = treatment), shape =21, width = 0.1, height = 0.1)+
  facet_wrap(~treatment) +
  theme_bw()

xtabs(~status + visit+treatment, data)
```



## 単純に回帰

年齢性別施設が共変量。治療の有無および繰り返し測定によって状態が異なり，これの交互作用も考慮する。

```{r}
model_0 <- glm(status ~ treatment*post + gender + age+centre, data, family = "binomial")

summary(model_0)
```

## ランダムエフェクトモデル

被験者ごとの変量効果が入るモデル

```{r}
library(lme4)
model_r <- lme4::glmer(status ~ treatment*post + gender + age_cen + centre + (1 | subject), 
                       data, family = "binomial", 
                       control = glmerControl(optimizer = "bobyqa", optCtrl = list(maxfun = 2e5))
                       )

summary(model_r)
```


## NPMLモデル

Longデータだと使えない！！
適用できるのは単純反復のWideデータのみ！！

```{r}
model_npml_novar <- alldist(
  status~1, random = ~1, data = respiratory, family = binomial(link = "logit"),
  k = 3, plot.opt = 0, random.distribution = "np"
  ) 
model_npml_novar
```
