# Automobileデータセット

## Source
 - https://archive.ics.uci.edu/ml/datasets/automobile

 1. 1985 Model Import Car and Truck Specifications, 1985 Ward's Automotive Yearbook.
 2. Personal Auto Manuals, Insurance Services Office, 160 Water Street, New York, NY 10038
 3. Insurance Collision Report, Insurance Institute for Highway Safety, Watergate 600, Washington, DC 20037

## Creator
 Jeffrey C. Schlimmer (Jeffrey.Schlimmer '@' a.gp.cs.cmu.edu)

## Data Set Information
1985年時点での北米市場における日本カーメーカーについて，馬力，燃費，価格を示したものである．ただし，元データから，ガソリン車，自然吸気，前輪駆動，OHCエンジン，4シリンダー，であるものに限定して抜粋している．

 - `imports-85.dat`: 元データ
 - `automobile.dat`: データ抜粋版
 - `automobile-short.dat`: データ抜粋版，データ数を大幅カット．

## Attribute Information:

1. `make`: toyota, honda, nissan
2. `horsepower`: horsepower in ps
3. `mileage`: mileage in km/L
4. `price`: price in doller

## Attributes types:
 Real: 2,3,4
 Nominal:1