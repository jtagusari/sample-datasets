# LOWBWTデータセット

## About

 - 低出生体重児に関するデータ
 - 出典：Applied Logistic Regression ISBN: 9780470582473 ( https://wiley.mpstechnologies.com/wiley/BOBContent/searchLPBobContent.do )


 ## 変数について

 1.  `ID`
 2.  `LOW`: 低出生体重か否か
   - `0`: 低出生体重ではない（体重2500g以上）
   - `1`: 低出生体重（体重2500g未満）
 3.  `AGE`: 母親の年齢
 4.  `LWT`: 最終月経における母親の体重 (単位はポンド)
 5.  `RACE`: 人種
   - `1`: 白人
   - `2`: 黒人
   - `3`: その他
 6.  `SMOKE`: 妊娠中の喫煙
   - `0`: 喫煙なし
   - `1`: 喫煙あり
 7.  `PTL`: 早産の経験の回数
 8.  `HT`: 高血圧の既往歴の有無
   - `0`: なし
   - `1`: あり
 9.  `UI`: 局所子宮内感染の有無
   - `0`: 無し
   - `1`: 有り
 10. `FTV`: 妊娠初期に内科医を訪れた回数
 11. `BWT`: 出生体重 (単位はグラム)

 
## Attribute Information:

 1.  `ID`: Identification code 1-189 
 2.  `LOW`: Low birth weight
   - `0`: >= 2500g
   - `1`: < 2500g
 3.  `AGE`: Age of mother (years)
 4.  `LWT`: Weight of mother at last menstrual period (pounds)
 5.  `RACE`: Race
   - `0`: white
   - `1`: black
   - `2`: others
 6.  `SMOKE`: Smoking status during pregnancy
   - `0`: No
   - `1`: Yes
 7.  `PTL`: History of premature labor
   - `0`: None
   - `1`: One
   - `2`: Two, etc.
 8.  `HT`: History of hypertension
   - `0`: No
   - `1`: Yes
 9.  `UI`: Presence of Uuterine irritability
   - `0`: No
   - `1`: Yes
 10. `FTV`: Number of physician visits during the first trimester
   - `0`: None
   - `1`: One
   - `2`: Two, etc.
 11. `BWT`: Recorded birth weight (grams)