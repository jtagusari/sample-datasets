NHANESデータ使用に関する諸注意
2019/6/25 by Junta Tagusari

データ形式について
 - データはタブで区切られています
 - 古いバージョンのWindowsメモ帳だと，改行コードがきちんと認識されない事がある様です．データの中身を眺めるには，別のソフトウェアか，新しいバージョンのWindowsを利用してください．

データ詳細
 - Code_Sheet等を参照してください．
 - 各変数の詳細については，NHANESのWebサイト等を参照してください．（https://wwwn.cdc.gov/nchs/nhanes/tutorials/default.aspx）