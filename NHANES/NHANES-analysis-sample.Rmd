---
title: "NHANES Data Analysis"
author: "Junta Tagusari"
output: 
  html_document:
    toc: yes
    toc_depth: 2
    toc_float: yes
    number_sections: true
editor_options: 
  chunk_output_type: console
---



# なにがしたいか
 - NHANESデータの解析

# 設定

## ライブラリ
```{r loadlib, message = F}
library(tidyverse)
library(GGally)
```

## セッション情報
```{r sessioninfo}
sessionInfo()
Sys.time()
```

# データ準備

```{r}
NHANES <- read_delim('NHANES/NHANES.txt', delim = "\t", col_types = "ddddddddddddddddddddd")%>%
  dplyr::mutate(
    AGE_ctg   = cut(AGE, breaks= c(0,30,40,50,60,70,Inf), include.lowest = T, right = F),
    TCHOL_ctg = cut(TCHOL, breaks = c(0,220,Inf), include.lowest = T, right = F),
    BMI_ctg   = cut(BMI, breaks = c(0,30,Inf), include.lowest = T, right = F),
    SYSBP_ctg   = cut(SYSBP, breaks = c(0,140,Inf), include.lowest = T, right = F),
    TCHOL,
    BMI,
    SYSBP,
    SEDMIN_ctg = cut(SEDMIN, breaks = c(0,150,300,450,Inf), include.lowest = T, right = F),
    GENDER    = factor(GENDER, labels = c('Male', 'Female')),
    MARSTAT   = factor(MARSTAT, labels = c('Married', 'Widowed', 'Divorced', 'Separated',
                                         'Never_married', 'Living Together')),
    
    VIGWRK    = factor(VIGWRK, labels = c('Yes', 'No')),
    MODWRK    = factor(MODWRK, labels = c('Yes', 'No')),
    WRKACT    = factor(if_else(VIGWRK=='No' & MODWRK == 'No', 'No', 'Yes')),
    
    WLKBIK    = factor(WLKBIK, labels = c('Yes', 'No')),
    
    VIGRECEXR = factor(VIGRECEXR, labels = c('Yes', 'No')),
    MODRECEXR = factor(MODRECEXR, labels = c('Yes', 'No')),
    RECEXR    = interaction(VIGRECEXR, MODRECEXR),
    
    OBESE     = factor(OBESE, labels = c('No', 'Yes'))
  )
summary(NHANES)
```

# スモールサンプル

## ランダム抽出
```{r}
set.seed(1)
NHANES_small <- 
  bind_rows(
    NHANES %>%
      na.omit() %>%
      dplyr::filter(OBESE=="Yes") %>%
      .[sample(1:nrow(.), 100),],
    NHANES %>%
      na.omit() %>%
      dplyr::filter(OBESE=="No") %>%
      .[sample(1:nrow(.), 100),]
  ) %>%
  dplyr::select(
    GENDER, AGE, MODRECEXR, BMI
  )

# write.csv(NHANES_small, file = "NHANES-small.dat")

summary(NHANES_small)

ggpairs(NHANES_small)
```

# 重回帰

```{r}
lm(BMI ~ GENDER + AGE_ctg + MODRECEXR, data = NHANES) %>% summary()
```

# ロジスティック回帰
```{r}
glm(OBESE ~ GENDER + AGE_ctg + MODRECEXR, data = NHANES, family = binomial) %>% summary()
glm(OBESE ~ AGE_ctg + GENDER*MODRECEXR, data = NHANES, family = binomial) %>% summary()
```


# まとめ

```{r}
ls.str()
```