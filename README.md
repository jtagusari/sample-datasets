# Rサンプルデータ

 - 色々役に立つであろうサンプルデータ
 - さらなる詳細は各フォルダ内の`README.md`に書いてあるかもしれない
 - 未整理データも格納
 - `git`の仕様上，色々と細かな変更をしないこと。

## データ
 - `dataframe-sample`: データフレーム読込練習用のサンプル
 - `heart`: 心筋梗塞に関するデータセット
 - `lowbwt`: 低出生体重児に関するデータセット
 - `automobile`: 米国での日本車の販売価格に関するデータセット
 - `disease-1`: 人工的に作ったデータセット，多変量回帰用？
 - `sample-disease`: 人工的に作ったデータセット
 - `boston`: ボストン住宅価格データ( https://www.cs.toronto.edu/~delve/data/boston/bostonDetail.html )．RだとMASSパッケージからも呼び出せる
 - `cancer`: がん患者の口腔環境に関するデータ( http://calcnet.mth.cmich.edu/org/spss/prj_cancer_data.htm )．
 - `statistical-test`：統計学的検定に関するいろいろな人工データ，Rmdファイルもセット。
   - `t-test.txt`：t検定（対応なし）
   - `wilcox-test.txt`：Wilcox検定（対応なし）
   - `wilcox-paired-test.txt`：符号付順位和検定（対応あり）
   - `cor-coef.txt`：相関係数（anscombeデータ）
   - `cor-coef-ctg.txt`：相関係数（二値データ）
   - `mcnemar-test`：Mcnemar検定
   - `friedman-test.txt`：Friedman検定（各野球選手における3種類の走塁法とタイム）
 - `respiratory`：ランダムエフェクト
