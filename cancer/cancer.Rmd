---
title: "Cancer data analysis"
author: "Junta Tagusari"
output: 
  html_document:
    toc: true
    toc_depth: 2
    toc_float:
      collapsed: false
    number_sections: true
editor_options: 
  chunk_output_type: console
---


# なにがしたいか
 - Cancerデータの解析例
 
# 設定

## ライブラリ
```{r loadlib, message = F}
library(tidyverse)
```

## セッション情報
```{r sessioninfo}
sessionInfo()
Sys.time()
```

# データ概要
( http://calcnet.mth.cmich.edu/org/spss/prj_cancer_data.htm )参照

アロエジュースによってがん患者の口腔環境が改善されるのでは？という仮説の検証

# データ読み込み

```{r}
data <- read_csv("cancer/cancer.csv", col_types = "ddddddddd") %>% 
  dplyr::rename(TOTALCW0 = TOTALCIN) %>% 
  dplyr::mutate(
    ID = factor(ID),
    WEIGHIN = WEIGHIN * 0.453592
    )

data
```


```{r}
data_long <- data %>% 
  tidyr::pivot_longer(starts_with("TOTAL"))

data_long
```

# データ分布確認

## 処置群`TRT`・ステージ`STAGE`

どのような被験者を対象としたのか？という問題
```{r}
xtabs(~TRT+STAGE, data)
```

## 年齢`AGE`
```{r}
ggplot(data, aes(AGE))+
  geom_histogram(boundary = 0, binwidth = 5)+
  theme_bw()
```


## 体重`WEIGHIN`
```{r}
ggplot(data, aes(WEIGHIN))+
  geom_histogram(boundary = 0, binwidth = 5)+
  theme_bw()
```

## 口腔環境

```{r}
ggplot(data_long, aes(name, value, group = ID, colour = ID))+
  geom_line()+
  geom_point(shape = 21) +
  facet_grid(TRT~.)+
  theme_bw()
```


# 二群の差の検定

`TRT`および`CTRL`群において，`TOTALCW0`と`TOTALCW6`の差を検定する

```{r}
data_trt <- dplyr::filter(data, TRT == 1)
data_ctrl <- dplyr::filter(data, TRT == 0)
```

## t検定

```{r}
t.test(Pair(TOTALCW0, TOTALCW6)~1, data_trt)
t.test(Pair(TOTALCW0, TOTALCW6)~1, data_ctrl)
```

## wilcoxon検定

```{r}
wilcox.test(Pair(TOTALCW0, TOTALCW6)~1, data_trt)
wilcox.test(Pair(TOTALCW0, TOTALCW6)~1, data_ctrl)
```

## Friedman検定

`TRT`および`CTRL`群において口腔環境の差を検定する
（この場合には順序尺度なので適切ではないのだが，練習。）

```{r}
friedman.test(as.matrix(dplyr::select(data_trt, starts_with("TOTAL"))))
friedman.test(as.matrix(dplyr::select(data_ctrl, starts_with("TOTAL"))))
```



# 二群の差の検定（名義）
`TOTALCW0`と`TOTALCW6`について，8以上で二値に分け，解析する

```{r}
data_trt_bin <- dplyr::filter(data, TRT == 1) %>% dplyr::mutate(across(starts_with("TOTAL"), ~.x >= 8)) 
data_ctrl_bin <- dplyr::filter(data, TRT == 0) %>% dplyr::mutate(across(starts_with("TOTAL"), ~.x >= 8))
```

```{r}
xtabs(~TOTALCW0+TOTALCW6, data_trt_bin) 
xtabs(~TOTALCW0+TOTALCW6, data_ctrl_bin)
```

## Mcnemar検定


```{r}
mcnemar.test(xtabs(~TOTALCW0+TOTALCW6, data_trt_bin) )
mcnemar.test(xtabs(~TOTALCW0+TOTALCW6, data_ctrl_bin) )
```