---
title: "メラノーマデータ"
author: "Junta Tagusari"
output: 
  html_document:
    toc: true
    toc_depth: 2
    toc_float:
      collapsed: false
    number_sections: true
editor_options: 
  chunk_output_type: console
---


# なにがしたいか
 - メラノーマ：皮膚がん
 
# 設定

## ライブラリ
```{r loadlib, message = F}
library(tidyverse)
library(MASS)
```

# データ

```{r}
data("Melanoma")
Melanoma <- as_tibble(Melanoma)
# write.csv(Melanoma, "melanoma/melanoma.csv", row.names = F)
```

```{r}
data <- read.csv("melanoma/melanoma.csv")
```