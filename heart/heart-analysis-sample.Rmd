---
title: "Heart Data Analysis Sample"
author: "Junta Tagusari"
output: 
  html_document:
    toc: yes
    toc_depth: 2
    toc_float: yes
    number_sections: true
editor_options: 
  chunk_output_type: console
---



# なにがしたいか
 - heartデータ(https://archive.ics.uci.edu/ml/datasets/Statlog+%28Heart%29)の解析
 - その他のデータの準備・解析

# 設定

## ライブラリ
```{r loadlib, message = F}
library(tidyverse)
library(GGally)
```

## セッション情報
```{r sessioninfo}
sessionInfo()
Sys.time()
```

# データ読み込み
 - 適切な型に変換しておく

```{r}
dat_hd <- read_delim("heart/heart.dat", delim = " ", col_types = "dddddddddddddd") %>%
  dplyr::mutate(
    sex = factor(sex, labels = c("female", "male")),
    cp  = factor(cp, labels = c("typ_ang", "atyp_ang", "non_ang", "asymp")),
    fbs = factor(fbs, labels = c("<120", ">120")),
    restecg = factor(restecg, labels = c("norm", "st_abnorm", "lv_hypt")),
    exang = factor(exang, labels = c("no", "yes")),
    slope = factor(slope, labels = c("up", "flat", "down")),
    thal  = factor(thal, labels= c("norm", "fdef", "rdef")),
    hd = factor(hd, labels = c("abs", "prs"))
  )
str(dat_hd)
summary(dat_hd)

```


# 一部データの解析

 - 血圧やコレステロールと心疾患の関係は？
 - プロットしてみるだけ


## 抽出
```{r}
dat_hd_ext <- dat_hd %>%
  dplyr::select(sbp, chol, hd) %>%
  dplyr::mutate(
    sbp_ctg = cut(sbp, breaks = c(0,120,140,160,Inf)),
    is_hd = hd == "prs"
  )

summary(dat_hd_ext)
```

## プロット

### pairs
```{r}
ggpairs(
  dat_hd_ext %>%
    dplyr::select(sbp, sbp_ctg, chol, is_hd)
)
```


# 重回帰

ST低下値`oldpeak`を胸痛`cp`，最高心拍`mthalach`で回帰する

## データ抽出・プロット
```{r}
dat_hd_op <- dat_hd %>%
  dplyr::select(cp, chol, mthalach, oldpeak) %>%
  dplyr::mutate(
    mthalach_ctg = cut(mthalach, breaks = c(0,140,160,180,Inf)),
    # age_ctg = cut(age, breaks = c(0,40,50,60,Inf)),
    # sbp_ctg = cut(sbp, breaks = c(0,120,140,160,Inf))
  )

summary(dat_hd_op)

ggpairs(select(dat_hd_op, -contains("ctg")))

```

## 重回帰モデル作成

```{r}
model_hd_op <- lm(oldpeak ~ cp + mthalach + cp + chol, data = dat_hd_op)

summary(model_hd_op)
```

## モデルによる予測

胸痛`cp`毎に予測・プロット

```{r}
dat_hd_op_blank <- tibble(cp = levels(dat_hd_op$cp)) %>%
  dplyr::mutate(x = purrr::map(cp, ~data.frame(chol = seq(120, 600,10)))) %>%
  tidyr::unnest(x) %>%
  dplyr::mutate(x = purrr::map(cp, ~data.frame(mthalach = seq(60,220,10)))) %>%
  tidyr::unnest(x)

# write.csv(dat_hd_op_blank, file = "heart-blank.dat", row.names = F)

dat_hd_op_pred <- cbind(dat_hd_op_blank,
                        predict(model_hd_op, dat_hd_op_blank, interval = "confidence"))

ggplot(
  dat_hd_op_pred %>%
    dplyr::filter(chol == 240),
  aes(mthalach, colour = cp, fill = cp)
) +
  geom_line(aes(y = fit)) +
  geom_ribbon(aes(ymin = lwr, ymax = upr), alpha = 0.2) +
  geom_point(data = dat_hd_op, aes(y = oldpeak)) +
  facet_wrap(~cp) +
  scale_x_continuous(breaks = seq(0,600,20)) +
  scale_y_continuous(breaks = seq(0,6,1)) +
  coord_cartesian(xlim = c(60,200), ylim = c(0,6.5)) +
  theme_bw()

# write.csv(dat_am_blank, file = "automobile/automobile-blank.dat")
```




# ロジスティック回帰

ST低下値について`oldpeak>1`をアウトカムとして多重ロジスティック回帰分析


## データ抽出
```{r}
dat_hd_op <- dat_hd %>%
  dplyr::select(cp, chol, mthalach, oldpeak) %>%
  dplyr::mutate(
    is_lowST = if_else(oldpeak > 1, T, F),
    mthalach_ctg = cut(mthalach, breaks = c(0,140,160,180,Inf)),
    # age_ctg = cut(age, breaks = c(0,40,50,60,Inf)),
    # sbp_ctg = cut(sbp, breaks = c(0,120,140,160,Inf))
  )


summary(dat_hd_op)

ggpairs(select(dat_hd_op, -contains("ctg")))
```

## モデル生成
```{r}
model_hd_op <- glm(is_lowST ~ cp + mthalach_ctg + cp + chol, data = dat_hd_op, family = binomial)
summary(model_hd_op)
```

## モデルに基づく予測
```{r}
dat_hd_op_pred <- dat_hd_op %>%
  dplyr::mutate(
    pred = predict(model_hd_op, type = "response"),
    resid = resid(model_hd_op)
    )

dat_hd_op_pred
```

# 心疾患`hd`についての多重ロジスティック回帰分析

## データ準備

```{r}
dat_hd_rev <- dat_hd %>%
  dplyr::transmute(
    is_hd = if_else(hd == "prs", T, F),
    age_ctg = cut(age, breaks = c(0,40,50,60,Inf)),
    sex,
    cp = relevel(cp, "non_ang"),
    sbp_bin = cut(sbp, breaks = c(0,140,Inf)),
    chol_bin = cut(chol, breaks = c(0,220,Inf)),
    fbs,
    restecg,
    mthalach_bin = cut(mthalach, breaks = c(0,140,Inf)),
    exang,
    oldpeak_bin = cut(oldpeak, breaks = c(0,2,Inf)),
    slope = cut(as.numeric(slope), breaks = c(0,1,Inf), label = c("up", "flatdown")),
    ca_bin = cut(ca, breaks = c(0,1,Inf), include.lowest = T, right = F),
    thal
  )

summary(dat_hd_rev)
xtabs(~oldpeak_bin + slope, data = dat_hd_rev)
```

## モデル生成



```{r}
model_hd_full <- glm(is_hd~age_ctg+sex+cp+sbp_bin+chol_bin+fbs+restecg+
      mthalach_bin+exang+oldpeak_bin*slope+ca_bin+thal, dat_hd_rev, family = binomial) 
summary(model_hd_full)

car::Anova(model_hd_full, test.statistic = "Wald")
broom::tidy(model_hd_op)
```

# まとめ

```{r}
ls()
```