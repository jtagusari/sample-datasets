# heartサンプルデータ

 - このデータセットは，以下URLに提供されたデータセットを一部修正したものです．（2022/1/11 by 田鎖）

## Source
 - https://archive.ics.uci.edu/ml/datasets/heart+Disease
 - http://archive.ics.uci.edu/ml/datasets/statlog+(heart)

## Creators
 1. Hungarian Institute of Cardiology. Budapest: Andras Janosi, M.D.
 2. University Hospital, Zurich, Switzerland: William Steinbrunn, M.D.
 3. University Hospital, Basel, Switzerland: Matthias Pfisterer, M.D.
 4. V.A. Medical Center, Long Beach and Cleveland Clinic Foundation: Robert Detrano, M.D., Ph.D.

## Donor
 David W. Aha (aha '@' ics.uci.edu) (714) 856-8779

## Data Set Information

 This database contains 13 attributes (which have been extracted from a larger set of 75). In particular, the Cleveland database is the only one that has been used by ML researchers to this date. The goal is to evaluate the contributions of the factors to the risks of heart disease in the patient. 
 There are 270 observations with no missing values.

## Attribute Information:
 1. `age`: age in years
 2. `sex`: sex
   - `0`: female
   - `1`: male
 3. `cp`: chest pain type 
   - `1`: typical angina
   - `2`: atypical angina
   - `3`: non-anginal pain
   - `4`: asymptomatic
4. `sbp`: systolic blood pressure (in mm Hg on admission to the hospital) 
5. `chol`: serum cholestoral in mg/dl 
6. `fbs`: (fasting blood sugar > 120 mg/dl)  
  - `0`: fbs <= 120 mg/dl
  - `1`: fbs >  120 mg/dl
 7. `restecg`: resting electrocardiographic results
   - `0`: normal
   - `1`: having ST-T wave abnormality (T wave inversions and/or ST elevation or depression of > 0.05 mV)
   - `2`: showing probable or definite left ventricular hypertrophy by Estes' criteria 
 8. `mthalach`: maximum heart rate achieved 
 9. `exang`: exercise induced angina
   - `0`: no
   - `1`: yes
 10. `oldpeak`: ST depression induced by exercise relative to rest 
 11. `slope`: the slope of the peak exercise ST segment
   - `1`: upsloping
   - `2`: flat
   - `3`: downsloping 
12. `ca`: number of major vessels (0-3) that appeared to contain calcium colored by flourosopy 
13. `thal`: excercise thallium scintigraphic defects
   - `3`: normal
   - `6`: fixed defect
   - `7`: reversable defect 
14. `hd`: absence or presence of heart disease
  - `1`: absence
  - `2`: presence

## 説明変数について
 1. `age`: 年齢
 2. `sex`: 性別
   - `0`: 女性
   - `1`: 男性
 3. `cp`: 胸部の痛み 
   - `1`: 狭心症
   - `2`: 異型狭心症
   - `3`: 狭心症由来ではない痛み
   - `4`: 無症状
4. `sbp`: 収縮期血圧 (mm Hg) 
5. `chol`: 血中コレステロール (mg/dl) 
6. `fbs`: 中性脂肪  
  - `0`: 120 mg/dl 以下
  - `1`: 120 mg/dl 超
 7. `restecg`: 安静時心電図の結果
   - `0`: 通常
   - `1`: ST-T波に異常あり (T 波が反転および／もしくは ST の上昇（もしくは低下）が 0.05 mV超)
   - `2`: 左心室肥大の可能性あり 
 8. `mthalach`: 最高心拍 
 9. `exang`: 運動誘発性狭心症
   - `0`: なし
   - `1`: あり
 10. `oldpeak`: 安静時と比較した際の，運動によるSTの低下 
 11. `slope`: 運動時のSTの傾き
   - `1`: 上昇
   - `2`: 平坦
   - `3`: 低下 
12. `ca`: 蛍光透視法によってカルシウムが検出された脈管数 (0-3)  
13. `thal`: タリウム運動負荷心筋シンチグラフィの異常
   - `3`: 正常
   - `6`: 不可逆的な異常
   - `7`: 可逆的な異常 
14. `hd`: 心疾患
  - `1`: なし
  - `2`: あり

## Attributes types
 - Real: 1,4,5,8,10,12
 - Ordered:11,
 - Binary: 2,6,9
 - Nominal:7,3,13