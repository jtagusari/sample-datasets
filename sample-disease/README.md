# Sample Disease Dataset

## Fields
- Age | age | int (years)
- Sex | sex | category (1: male / 2: female)
- Address of the subject | region | category (A, B, C)
- Height | height | float (cm) 
- Weight | weight | float (kg) 
- Daily intake of alcohol | alc | category (1: <20g/day, 2: 20-40g/day, 3: 40-60g/day, 4: >60g/day)
- Concentration of cotinine in blood plasma | tob | float (ng/ml)
- Index to identify disease | disease_idx | float
- Prevalence of disease | disease | binary (Is the disease present? 0: No, 1: Yes)

## NOTE
- This is just a sample dataset but not represent any actual diseases

By jtagusari, 2022/12/9